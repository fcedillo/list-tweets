require 'mysql'

INVALID_MAX_ID = 0

# Gets max ID from tweets table
def get_db_max_id()
  
  con = Mysql.new 'localhost', 'tweetsdbuser', 't2w0e0e4', 'tweetsdb'

  rs = con.query("SELECT id FROM tweets LIMIT 1") 

  max_id = INVALID_MAX_ID

  if (rs.num_rows > 0)
    max_id = rs.fetch_row.first.to_i
  end

  con.close if con

  return max_id

end


# Returns max ID
# Verifies if tweets table is empty
def get_max_id()

  base_max_id = 348183814268407808
  db_max_id = get_db_max_id()

  if (db_max_id > INVALID_MAX_ID)
    max_id = db_max_id
  else
    max_id = base_max_id
  end

  return max_id

end

