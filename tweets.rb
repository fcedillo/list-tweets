require 'rubygems'
require 'oauth'
require 'json'
load 'tweets-to-db.rb'
load 'get-max-id.rb'

# Now you will fetch /1.1/statuses/user_timeline.json,
# returns a list of public Tweets from the specified
# account.
baseurl = "https://api.twitter.com"
path    = "/1.1/lists/statuses.json"

max_id = get_max_id()

# https://twitter.com/ParaCuerdas/mascotas-proyectos
query   = URI.encode_www_form(
    "owner_screen_name" => "ParaCuerdas",
    "slug" => "mascotas-proyectos",
    "since_id" => max_id,

)
address = URI("#{baseurl}#{path}?#{query}")
request = Net::HTTP::Get.new address.request_uri

# Set up HTTP.
http             = Net::HTTP.new address.host, address.port
http.use_ssl     = true
http.verify_mode = OpenSSL::SSL::VERIFY_PEER

# If you entered your credentials in the first
# exercise, no need to enter them again here. The
# ||= operator will only assign these values if
# they are not already set.
consumer_key ||= OAuth::Consumer.new "Jg8L5yS0pu83iX8WAZDopA", "HYxEPAQ1OhlCkCMgfikOdNGzUGBKjz8vp5TT0ZpHrQ"
access_token ||= OAuth::Token.new "11880122-5hGYfCmqmxHG6D00HzIN7LVIcq64FbOA7XIzBrank", "WfNq2Yf87A8PfTESbmQjk9RnfYtdEQPkftK8TrJQkA"

# Issue the request.
request.oauth! http, consumer_key, access_token
http.start
response = http.request request


#File.open("tweets/tweets.json","w") do |f|
#  f.write(response.body)
#end


# Parse and print the Tweet if the response code was 200
tweets = nil
if response.code == '200' then
  tweets = JSON.parse(response.body)
  tweets_to_db(tweets)
end
nil
