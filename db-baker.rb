require 'mysql'

# Print tweets in database
def print_database_tweets()
  
  con = Mysql.new 'localhost', 'tweetsdbuser', 't2w0e0e4', 'tweetsdb'

  rs = con.query("SELECT text FROM tweets") 
  n_rows = rs.num_rows
    
  puts "There are #{n_rows} rows in the result set"
    
  n_rows.times do
    puts "text: " + rs.fetch_row.join("\s")
  end

  con.close if con

end

=begin
SELECT text, LENGTH( text )
FROM `tweets`
WHERE 1
LIMIT 0 , 30

SELECT `created_at` , LENGTH( `created_at` )
FROM `tweets`
WHERE 1
LIMIT 0 , 30
30

SELECT `id_str` , LENGTH( `id_str` )
FROM `tweets`
WHERE 1
LIMIT 0 , 30
18

SELECT MAX(LENGTH(user_id_str)) FROM tweets
9
=end

