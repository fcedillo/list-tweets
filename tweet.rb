require 'rubygems'
require 'oauth'
require 'json'

# Now you will fetch /1.1/statuses/show.json, which
# takes an 'id' parameter and returns the
# representation of a single Tweet.
baseurl = "https://api.twitter.com"
path    = "/1.1/statuses/show.json"
query   = URI.encode_www_form("id" => "266270116780576768")
address = URI("#{baseurl}#{path}?#{query}")
request = Net::HTTP::Get.new address.request_uri

# Print data about a Tweet
def print_tweet(tweet)
  # ADD CODE TO PRINT THE TWEET IN "<screen name> - <text>" FORMAT
  puts tweet["user"]["name"] + ' - ' + tweet["text"]
end

# Set up HTTP.
http             = Net::HTTP.new address.host, address.port
http.use_ssl     = true
http.verify_mode = OpenSSL::SSL::VERIFY_PEER

# If you entered your credentials in the first
# exercise, no need to enter them again here. The
# ||= operator will only assign these values if
# they are not already set.
consumer_key ||= OAuth::Consumer.new "Jg8L5yS0pu83iX8WAZDopA", "HYxEPAQ1OhlCkCMgfikOdNGzUGBKjz8vp5TT0ZpHrQ"
access_token ||= OAuth::Token.new "11880122-5hGYfCmqmxHG6D00HzIN7LVIcq64FbOA7XIzBrank", "WfNq2Yf87A8PfTESbmQjk9RnfYtdEQPkftK8TrJQkA"

# Issue the request.
request.oauth! http, consumer_key, access_token
http.start
response = http.request request

# Parse and print the Tweet if the response code was 200
tweet = 348244264616071168
# 348183814268407808
if response.code == '200' then
  tweet = JSON.parse(response.body)
  print_tweet(tweet)
end
