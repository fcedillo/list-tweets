require 'mysql'

# Print data about a list of Tweets
def tweets_to_db(tweets)
  
  con = Mysql.new 'localhost', 'tweetsdbuser', 't2w0e0e4', 'tweetsdb'
  
  # ADD CODE TO ITERATE THROUGH EACH TWEET AND PRINT ITS TEXT
  tweets.each do |tweet|

    created_at = tweet['created_at']
    id = tweet['id']
    text = tweet['text']
    user_id_str = tweet['user']['id_str']
    lang = tweet['lang']
    
    escaped_text = con.escape_string(text)

    con.query("INSERT INTO tweets(created_at, id, text, lang, user_id_str) VALUES('#{created_at}', #{id}, '#{escaped_text}', '#{lang}', '#{user_id_str}')") 
  end

  con.close if con

end
