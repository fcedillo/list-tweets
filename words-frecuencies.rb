# encoding: utf-8
# word frecuency


def line_punctuation_cleanup(line)
  line.tr(',.!¡():@/', '')
end


def line_split(line)
  line.split(" ")
end


def word_acute_characters_cleanup(line)
  line.tr('ÁÉÍÓÚáéíóú', 'aeiouaeiou')
end

def is_a_meaningful_word(word)
  not_meaningful_words_en = ['you', 'your', 'do', 'for', '»', 'and', 'a', 'in', 'from', 'out', 'of', 'the', 'is', 'this', 'we', 'how', 'more', 'with', 'after', 'who', 'to', 'are', 'w']
  not_meaningful_words_es = ['en', 'y', '»', 'de', 'su', 'las', 'aqui', 'del', 'para', 'los', 'rt', 'es', 'un', 'el', 'la', 'que', 'tu', 'me', 'lo', 'no', '-', 'se', 'ser', 'por', 'via', 'mas', 'al', 'con', 'q', 'tus', 'pero', '---&gt;', 'aqui', 'le', 'hoy', 'si', 'esta', 'hay', 'asi', 'una', 'dime', 'mi', 'pues', 'te' ]
  return !( not_meaningful_words_en.include?(word.downcase) or not_meaningful_words_es.include?(word.downcase) )
end


def words_counter(words_array)
  count_hash = Hash.new
  words_array.each {
    |word|
    cleaned_word = word_acute_characters_cleanup(word)
    if (is_a_meaningful_word(cleaned_word))
      if ( count_hash.has_key?(cleaned_word) )
        count_hash[cleaned_word] += 1
        # puts "cleaned_word #{cleaned_word} is in count_array. #{count_hash[cleaned_word]}"
      else
        count_hash[cleaned_word] = 1
        # puts "iteration cleaned_word: count_hash #{count_hash}"
      end
    end
  }
  return count_hash
end


def words_frecuencies(line)

  cleaned_line = line_punctuation_cleanup(line)
  # puts "cleaned_line: #{cleaned_line}"
  
  splitted_line = line_split(cleaned_line)
  # puts "splitted_line: #{splitted_line}"
  
  sorted_count_hash = words_counter(splitted_line)
  # puts "sorted_count_hash: #{sorted_count_hash}"

end


def words_frecuencies_in_lines(lines)
  
  total_frecuencies = Hash.new
  
  lines.each {
    |line|
    words_frecuencies = words_frecuencies(line)
    total_frecuencies = total_frecuencies.merge(words_frecuencies){|key, oldval, newval| newval + oldval}
  }

  total_frecuencies_not_once_words = total_frecuencies.delete_if {| word, count | count == 1 }
  
  sorted_total_frecuencies = total_frecuencies_not_once_words.sort_by{ |word, count| count }.reverse

end


def print_words_frecuencies(words_frecuencies_in_lines)
  words_frecuencies_in_lines.each {
    |word, frecuency|
    puts "#{word}: #{frecuency}"
  }
end


=begin
line = "hola casa, casa   ¡cómo  estas!. hola,   como casa!"
line2 = "hey, loco casa casa! cása, como comó"
line3 = "casa come como loco harta comida, eso!"


lines = [line, line2, line3]


words_frecuencies_in_lines = words_frecuencies_in_lines(lines)


print_words_frecuencies(words_frecuencies_in_lines)
=end


