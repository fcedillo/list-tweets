require 'mysql'

# Print tweets in database
def print_database_tweets()
  
  con = Mysql.new 'localhost', 'tweetsdbuser', 't2w0e0e4', 'tweetsdb'

  rs = con.query("SELECT text FROM tweets") 
  n_rows = rs.num_rows
    
  puts "There are #{n_rows} rows in the result set"
    
  n_rows.times do
    puts "text: " + rs.fetch_row.join("\s")
  end

  con.close if con

end
