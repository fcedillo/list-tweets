require 'sinatra'
load 'list-tweets.rb'

get '/' do
  "List tweets"
end

get '/frecuencies/:list_slug' do
  @word_frecuencies = list_tweets(params[:list_slug])
  erb :'frecuencies'
end

get '/hello/:name' do
  "Hello there, #{params[:name]}."
end

get '/hello/:name/:city' do
  "Hey there #{params[:name]} from #{params[:city]}."
end

get '/form' do  
  erb :form  
end

post '/form' do
  "You said '#{params[:message]}'"
end

